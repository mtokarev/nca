# Build & launch

## Using Docker

```
$ docker build -t nca .
$ docker run -p 8000:8000 nca
```

## Without Docker

```
$ virtualenv --python=python3.5 .venv
$ source .venv/bin/activate
(.venv) $ pip install -r requirements.txt
(.venv) $ python manage.py runserver
```


the app available here: http://localhost:8000/
