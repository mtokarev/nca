import math
import json

from django.http.response import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_safe, require_POST


@require_safe
def index(request):
    return render(request, 'index.html')


@require_POST
def validate(request):
    data = json.loads(request.body.decode('utf-8'))

    x1 = data['click']['x']
    y1 = data['click']['y']
    x2 = data['circle']['x']
    y2 = data['circle']['y']

    radius = data['circle']['r']
    distance = math.hypot(x1 - x2, y1 - y2)
    result = distance < radius

    return JsonResponse({'result': result})
