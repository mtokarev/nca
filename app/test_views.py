
import json
import pytest


class TestAPI:

    @pytest.fixture()
    def circle_data(self):
        return {'x': 100, 'y': 100, 'r': 50}

    @pytest.mark.parametrize('x,y,expected', [
        (0, 0, False),
        (100, 200, False),
        (200, 100, False),
        (200, 200, False),
        (100, 100, True),
        (120, 120, True),
        (80, 80, True),
    ])
    def test_validate_success(self, x, y, expected, circle_data, client):

        data = {
            'click': {'x': x, 'y': y},
            'circle': circle_data
        }
        response = client.post('/api/validate',
                               data=json.dumps(data),
                               content_type='application/json')

        assert response.status_code == 200

        content = json.loads(response.content.decode('utf-8'))
        assert content == {'result': expected}
