
var app = angular.module('app', ['ngResource']);

app.service('validationService', function($resource) {
    return $resource('/api/validate', {}, {'validate': {'method': 'POST'}})
});

app.controller('CanvasController', function($scope, validationService) {

    $scope.canvas = document.getElementById('canvas');
    $scope.context = $scope.canvas.getContext('2d');

    $scope.click = function(event) {
        var rect = $scope.canvas.getBoundingClientRect();
        var x = event.clientX - rect.left;
        var y = event.clientY - rect.top;
        var data = {
            'circle': {'x': $scope.centerX, 'y': $scope.centerY, 'r': $scope.radius},
            'click': {'x': x, 'y': y}
        }
        validationService.validate(data, function(response) {
            var result = response['result'];
            $scope.context.beginPath();
            $scope.context.arc(x, y, 3, 0, 2 * Math.PI);
            $scope.context.fillStyle = result ? '#0e0' : '#e00';
            $scope.context.strokeStyle = result ? '#0e0' : '#e00';
            $scope.context.fill();
            $scope.context.stroke();
        });
    }

    $scope.draw = function() {
        $scope.centerX = Math.round(Math.random() * canvas.width);
        $scope.centerY = Math.round(Math.random() * canvas.height);
        $scope.radius = Math.round(Math.random() * canvas.height / 2);

        $scope.context.clearRect(0, 0, canvas.width, canvas.height);
        $scope.context.beginPath();
        $scope.context.arc($scope.centerX, $scope.centerY, $scope.radius, 0, 2 * Math.PI);
        $scope.context.strokeStyle = '#333';
        $scope.context.stroke();
    }

    // setup
    $scope.context.width = 600;
    $scope.context.height = 400;
    $scope.draw();
});
