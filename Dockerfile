FROM python:3.5-onbuild
EXPOSE 8000
RUN ["pytest"]
CMD ["python", "manage.py", "runserver", "0:8000"]
